---
title: "Task 1"
author: "Mikhail Zotov (661)"
date: '22 ноября 2016 г '
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

## Input

Input data:

```{r echo=TRUE}
N = 50;
p = 1/9;
a = 80;
b = 10;
M = (a*p + (-b)*(1-p))*N;
D = (a*a*p + (-b)*(-b)*(1-p) - M*M) * N;

print(M)
print(D)
```



## Histogram and curves

Histogram created for experiments (total prices recorded to results array):

```{r pressure, echo=FALSE}
results = c()

for (i in 1:N) {
  experiments = runif(70, 0, 1)
  price = 0
  
  #Experiment
  for (value in experiments) {
    profit = 0
    if (value <= 1/9) {
      profit = 80
    } else {
      profit = -10
    }
    price = price + profit
  }
  
  results = c(results, price)
}

avg = mean(results)
dispersion = var(results) #var

#построить графики по теоретич и эмперич оценкам
histogram = hist(results,freq=FALSE)
experimental  = density(results, from = histogram$breaks[1], to = histogram$breaks[length(histogram$breaks)]) #or from\to another
#experimental  = density(results) #or from\to another
lines(experimental, col="red", lwd=2)
theoretical = curve(dnorm(x,mean(results),sd(results)), lwd=2, col="darkblue", add=TRUE)
```

```{r echo=TRUE}
print(results)
print(avg)
print(dispersion)
```

Shapiro test:
```{r echo=TRUE}
shap = shapiro.test(results)
print(shap)
```


Chi square criteria for actual values with standard function (avg, dispersion):
```{r echo=TRUE}
summ = 0
for(i in 2:length(histogram$breaks)) {
  p_i = pnorm(histogram$breaks[i], avg, sqrt(dispersion)) - pnorm(histogram$breaks[i - 1], avg, sqrt(dispersion))
  v_i = histogram$counts[i - 1]
  
  summ = summ + ((v_i - N * p_i)**2 / (N * p_i))
}

print(summ)
```

Chi square criteria for theoretical values (M, D):
```{r echo=TRUE}
summ = 0
for(i in 2:length(histogram$breaks)) {
  p_i = pnorm(histogram$breaks[i], M, sqrt(D)) - pnorm(histogram$breaks[i - 1], M, sqrt(D))
  v_i = histogram$counts[i - 1]
  
  summ = summ + ((v_i - N * p_i)**2 / (N * p_i))
}

print(summ)
```

## Interval probabilities and statistics counts

Probabitily for the intervals with given M and D: 
```{r echo=TRUE}
print(pnorm(-200, M, sqrt(D)))
print(1 - pnorm(200, M, sqrt(D)))
print(pnorm(100, M, sqrt(D)) - pnorm(-100, M, sqrt(D)))
```


Count of the statistics:
```{r echo=TRUE}
looseMoreThan200 = 0
profitMoreThan200 = 0
profitBetween100 = 0

for (result in results) {
  if(result < -200) {
    looseMoreThan200 = looseMoreThan200 + 1
  } else if (result > 200){
    profitMoreThan200 = profitMoreThan200 + 1
  } else if (result < 100 && result > -100){
    profitBetween100 = profitBetween100 + 1
  }
}

print(looseMoreThan200)
print(profitMoreThan200)
print(profitBetween100)
```