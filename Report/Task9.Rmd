---
title: "Factor analysis"
author: "Mikhail Zotov (661)"
date: '28 декабря 2016 г '
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

##Чтение данных
```{r echo=TRUE}
data = read.delim("C:/RStudio/projects/Task4/minimumA/addicts.txt")
```

```{r echo=TRUE}

dataFrame = na.omit(data.frame(age = data$age, med = data$asi1_med, emp = data$asi2_emp, alc = data$asi3_alc,
                       dr = data$asi4_dr, leg = data$asi5_leg, fam = data$asi6_fam, psy = data$asi7_psy, dyr = data$asid3_dyr,
                       rabdru = data$rabdru))



fit = princomp(dataFrame)
summary(fit)
loadings(fit)
plot(fit, type="lines")
biplot(fit)

load = fit$loadings[,1:2]
plot(load, type="n")
text(load,labels=names(dataFrame), cex=.7)
#age, dyr, rabdru




sc = fit$scores[,1:2]
plot(sc, type="n")
text(sc,labels=row.names(sc), cex=.7)
```
